# Insurance Web Component

This project serves as a web component for [Micro Frontend Host](https://bitbucket.org/nhatpq304/microfrontendhost/src/master) project.

## Build

Run `npm run deliver` to build the project and create a web component script in root directory.

## Start

Uncomment `bootstrap` in `app.module.ts`.

Run `npm run start` to run the project.

